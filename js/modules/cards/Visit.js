'use strict'
export class Visit{
    constructor(request, root, id, doctor, visitDate, purpose, description, urgency, firstName, lastName, middleName) {
        this.request = request;
        this.root = root;
        this.id = id;
        this.doctor = doctor;
        this.visitDate = visitDate;
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.initializaton();
    }

    initializaton(){
        this.card = document.getElementById('card').content.children[0].cloneNode(true);
        this.fieldsRoot = this.card.getElementsByClassName('card__fields')[0];
        this.cardFieldTemplate = this.card.getElementsByClassName('card__prop')[0];
        this.cardValueTemplate = this.card.getElementsByClassName('card__value')[0];
        this.titleNode = document.createElement('span');
        this.titleNode.classList.add('card__title');
        this.cardFieldTemplate = document.createElement('div');
        this.cardFieldTemplate.classList.add('card__prop');
        this.cardValueTemplate = document.createElement('div');
        this.cardValueTemplate.classList.add('card__value');
        this.deleteVisitButton = this.card.getElementsByClassName('delete-card-button')[0];
        this.deleteVisitButton.addEventListener('click', this.deleteVisit);
        this.visitFormChangeButton = this.card.getElementsByClassName('change-card-button')[0];
        this.visitFormChangeButton.addEventListener('click', this.changeVisit);
        this.render();
    }

    render = () => {
        const visitDateField = this.cardFieldTemplate.cloneNode(true);
        visitDateField.textContent = 'Visit date:';
        this.fieldsRoot.appendChild(visitDateField);
        const visitDateValue = this.cardValueTemplate.cloneNode(true);
        visitDateValue.textContent = this.visitDate;
        this.fieldsRoot.appendChild(visitDateValue);

        const purposeField = this.cardFieldTemplate.cloneNode(true);
        purposeField.textContent = 'Purpose:';
        this.fieldsRoot.appendChild(purposeField);
        const purposeValue = this.cardValueTemplate.cloneNode(true);
        purposeValue.textContent = this.purpose;
        this.fieldsRoot.appendChild(purposeValue);

        const descriptionField = this.cardFieldTemplate.cloneNode(true);
        descriptionField.textContent  = 'Description:';
        this.fieldsRoot.appendChild(descriptionField);
        const descriptionValue = this.cardValueTemplate.cloneNode(true);
        descriptionValue.textContent  = this.description;
        this.fieldsRoot.appendChild(descriptionValue);

        const urgencyField = this.cardFieldTemplate.cloneNode(true);
        urgencyField.textContent  = 'Urgency:';
        this.fieldsRoot.appendChild(urgencyField);
        const urgencyValue = this.cardValueTemplate.cloneNode(true);
        urgencyValue.textContent  = this.urgency;
        this.fieldsRoot.appendChild(urgencyValue);

        const firstNameField = this.cardFieldTemplate.cloneNode(true);
        firstNameField.textContent  = 'Firs Name:';
        this.fieldsRoot.appendChild(firstNameField);
        const firstNameValue = this.cardValueTemplate.cloneNode(true);
        firstNameValue.textContent  = this.firstName;
        this.fieldsRoot.appendChild(firstNameValue);

        const lastNameField = this.cardFieldTemplate.cloneNode(true);
        lastNameField.textContent  = 'Last Name:';
        this.fieldsRoot.appendChild(lastNameField);
        const lastNameValue = this.cardValueTemplate.cloneNode(true);
        lastNameValue.textContent  = this.lastName;
        this.fieldsRoot.appendChild(lastNameValue);

        const middleNameField = this.cardFieldTemplate.cloneNode(true);
        middleNameField.textContent  = 'Middle Name:';
        this.fieldsRoot.appendChild(middleNameField);
        const middleNameValue = this.cardValueTemplate.cloneNode(true);
        middleNameValue.textContent  = this.middleName;
        this.fieldsRoot.appendChild(middleNameValue);
        this.root.appendChild(this.card);
    }

    deleteVisit = () => {
        const resp = this.request.delete(this.id);
        resp.then(resp => {
            if (resp.statusText == 'OK'){
                this.card.parentNode.removeChild(this.card);
            }
        })
    }
}
export class VisitCardiologist extends Visit{
    constructor(request, root, id, doctor, purpose, description, urgency, firstName, lastName, middleName, waightIndex, pressure, diseases, age) {
        super(request, root, id, doctor, purpose, description, urgency, firstName, lastName, middleName);
        this.pressure = pressure;
        this.waightIndex = waightIndex;
        this.diseases = diseases;
        this.age = age;
        this.title = 'Визит к кардиологу';
        this.render();
    }

    render = () =>{
        this.card.classList.add('cardiologist-card');
        this.titleNode.textContent = 'Визит к кардиологу';
        this.fieldsRoot.parentNode.prepend(this.titleNode);

        const pressureField = this.cardFieldTemplate.cloneNode(true);
        pressureField.textContent = 'Pressure';
        this.fieldsRoot.appendChild(pressureField);
        const pressureValue = this.cardValueTemplate.cloneNode(true);
        pressureValue.textContent = this.pressure;
        this.fieldsRoot.appendChild(pressureValue);

        const waightIndexField = this.cardFieldTemplate.cloneNode(true);
        waightIndexField.textContent = 'Waight Index';
        this.fieldsRoot.appendChild(waightIndexField);
        const waightIndexValue = this.cardValueTemplate.cloneNode(true);
        waightIndexValue.textContent = this.waightIndex;
        this.fieldsRoot.appendChild(waightIndexValue);

        const diseasesField = this.cardFieldTemplate.cloneNode(true);
        diseasesField.textContent = 'Diseases';
        this.fieldsRoot.appendChild(diseasesField);
        const diseasesValue = this.cardValueTemplate.cloneNode(true);
        diseasesValue.textContent = this.diseases;
        this.fieldsRoot.appendChild(diseasesValue);

        const ageField = this.cardFieldTemplate.cloneNode(true);
        ageField.textContent = 'Age';
        this.fieldsRoot.appendChild(ageField);
        const ageValue = this.cardValueTemplate.cloneNode(true);
        ageValue.textContent = this.age;
        this.fieldsRoot.appendChild(ageValue);

        this.root.appendChild(this.card);
    }
    }
    export class VisitDentist extends Visit{
        constructor(request, root, id, doctor, purpose, description, urgency, firstName, lastName, middleName, lastVisitDate) {
            super(request, root, id, doctor, purpose, description, urgency, firstName, lastName, middleName);
            this.lastVisitDate = lastVisitDate;
            this.title = 'Визит к стоматологу';
            this.render();
        }

        render = () =>{
            this.card.classList.add('dentist-card');
            this.titleNode.textContent = 'Визит к стоматологу';
            this.fieldsRoot.parentNode.prepend(this.titleNode);
        }
    }
    export class VisitTherapist extends Visit{
        constructor(root, doctor, purpose, description, urgency, firstName, lastName, middleName, age) {
            super(root, doctor, purpose, description, urgency, firstName, lastName, middleName);
            this.age = age;
            this.title = 'Визит к терапевту';
            this.render();
        }

        render = () =>{
            this.card.classList.add('therapist-card');
            this.titleNode.textContent = 'Визит к терапевту';
            this.fieldsRoot.parentNode.prepend(this.titleNode);
        }
    }
