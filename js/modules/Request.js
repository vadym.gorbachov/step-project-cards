'use strict'
export default class Request{
    constructor(url) {
        this.url = url;
    }

    setTokin(token){
        localStorage.setItem('token', token);
        this.token = token;
    }

    delete(id) {
        return this.fetchMethod('delete', '', id);
    }

    put(endpoint, data, id) {
        return this.fetchMethod();
    } // @todo Need to implement

    getAll() {
        return this.fetchMethod('GET');
    }

    post(data, endpoint) {
        return this.fetchMethod('post', endpoint, "", data);
    }

    getToken(login, password){
            return   fetch("https://ajax.test-danit.com/api/v2/cards/login", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email: login, password: password })
            })
                .then(response => response)
    }

    fetchMethod = (method, endpoint = '', id = '', data = null) => {
        return fetch(this.url + endpoint + "/" + id, {
            method: method,
            body: data,
            headers: { "Content-Type": "application/json",
            'Authorization': `Bearer ${this.token}`
         },
        });
    }
}
