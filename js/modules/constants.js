'use strict'
const root = document.getElementById('root');
const BASE_URL = 'https://ajax.test-danit.com/api/v2/cards';
const doctorsTypes = {CARDIOLOGIST: 'Cardiologist', DENTIST: 'Dentist', THERAPAPIST: 'Therapist'}

export  {
    root,
    BASE_URL,
    doctorsTypes
};
