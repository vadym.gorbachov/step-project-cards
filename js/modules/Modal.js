'use strict'
export default class Modal {
    constructor(root, request, doctorsTypes, VisitCardiologist, VisitDentist, VisitTherapist
        ) {
        this.CARDIOLOGIST = doctorsTypes.CARDIOLOGIST;
        this.DENTIST = doctorsTypes.DENTIST;
        this.THERAPAPIST = doctorsTypes.THERAPAPIST;
        this.root = root;
        this.request = request;
        this.VisitCardiologist = VisitCardiologist;
        this.VisitDentist = VisitDentist;
        this.VisitTherapist = VisitTherapist;
        this.visits = [];
        this.initialization();
    }

    initialization = () => {
        this.doctor = this.CARDIOLOGIST;
        this.board = document.getElementById('board').cloneNode(true).content.children[0];
        this.cardBord = this.board.getElementsByClassName('cards')[0];
        this.createVisitButton = this.board.getElementsByClassName('board__create-card-button')[0];
        this.createVisitButton.addEventListener('click', this.createVisit);
        this.filterVisitsButton = this.board.getElementsByClassName('board__filter-card-button')[0];
        this.filterVisitsButton.addEventListener('click', this.showFilterForm);
        this.visitForm = document.getElementById('visit-form').cloneNode(true).content.children[0];
        this.closeVisitForm = this.visitForm.getElementsByClassName('visit-form__close')[0];
        this.closeVisitForm.addEventListener('click', this.closeVisit);
        this.visitFormDoctor = this.visitForm.getElementsByClassName('visit-form__doctor')[0];
        this.visitFormDoctor.addEventListener('change', this.showRelevantFields);
        this.visitFormVisitDate = this.visitForm.getElementsByClassName('visit-date')[0];
        this.visitFormPurpose = this.visitForm.getElementsByClassName('visit-form__purpose')[0];
        this.visitFormDescription = this.visitForm.getElementsByClassName('visit-form__description')[0];
        this.visitFormUrgency = this.visitForm.getElementsByClassName('visit-form__urgency')[0];
        this.visitFormName = this.visitForm.getElementsByClassName('visit-form__name')[0];
        this.visitFormSurname = this.visitForm.getElementsByClassName('visit-form__surname')[0];
        this.visitFormMiddleName = this.visitForm.getElementsByClassName('visit-form__middle-name')[0];
        this.visitFormPressureBottomLabel = this.visitForm.getElementsByClassName('visit-form__pressure-label')[0];
        this.visitFormPressureBottom = this.visitForm.getElementsByClassName('visit-form__pressure-bottom')[0];
        this.visitFormPressureTop = this.visitForm.getElementsByClassName('visit-form__pressure-top')[0];
        this.visitFormBodyMassIndexLabel = this.visitForm.getElementsByClassName('visit-form__body-mass-index-label')[0];
        this.visitFormBodyMassIndex = this.visitForm.getElementsByClassName('visit-form__body-mass-index')[0];
        this.visitFormCardioTroublesLabel = this.visitForm.getElementsByClassName('visit-form__cardio-troubles-label')[0];
        this.visitFormCardioTroubles = this.visitForm.getElementsByClassName('visit-form__cardio-troubles')[0];
        this.visitFormAgeLabel = this.visitForm.getElementsByClassName('visit-form__Age-label')[0];
        this.visitFormAge = this.visitForm.getElementsByClassName('visit-form__Age')[0];
        this.visitFormLastVisitDateLabel = this.visitForm.getElementsByClassName('visit-form__last-visit-label')[0];
        this.visitFormLastVisitDate = this.visitForm.getElementsByClassName('visit-form__last-visit')[0];
        this.visitFormLastVisitDateLabel.style.display = 'none';
        this.visitFormLastVisitDate.style.display = 'none';
        this.visitFormCreateButton = this.closeVisitForm.parentNode.parentNode.getElementsByClassName('container')[0];
        this.visitFormCreateButton.addEventListener('click', this.createCard); ///////////
        this.filterForm = document.getElementById('filter-form').content.children[0];
        this.filterFormTitle = this.filterForm.getElementsByClassName('filter-form-title')[0];
        this.filterFormUrgency = this.filterForm.getElementsByClassName('filter-form-urgency')[0];
        this.filterFormDateFrom = this.filterForm.getElementsByClassName('visit-date-from')[0];
        this.filterFormDateTo = this.filterForm.getElementsByClassName('visit-date-to')[0];
        this.filterBoardNav = this.board.getElementsByClassName('board__fixed-menu')[0];
        this.filterFormSubmit = this.filterForm.getElementsByClassName('filter-submit')[0];
        this.filterFormSubmit.addEventListener('click', this.filterVisits);
    }

    showFilterForm = () => {
      this.filterBoardNav.appendChild(this.filterForm);
    }

  filterVisits = () =>{
    this.filterForm.parentNode.removeChild(this.filterForm);
      let filteredVisits = this.visits;
      if(this.filterFormTitle.value !== 'All'){
        filteredVisits = this.filterByTitle(filteredVisits, this.filterFormTitle.value);
        console.log(`title ${filteredVisits}`)
      };

      if(this.filterFormUrgency.value !== 'All'){
        filteredVisits = this.filterByUrgency(filteredVisits, this.filterFormUrgency.value)
        console.log(`urgency ${filteredVisits}`)
      }

    filteredVisits = this.filterByDate(filteredVisits, this.filterFormDateFrom, this.filterFormDateTo);

    for (const visit of this.visits) {
      visit.card.style.display = 'none';
    }

    for (const visit of filteredVisits) {
      visit.card.style.display = 'block';
    }
    }

    filterByTitle(visits, title){
      return visits.filter(visit => visit.title == title);
    }

    filterByUrgency(visits, urgency){
      return visits.filter(visit => visit.urgency == urgency);
    }

    filterByDate(visits, dateFrom, dateTo){
      dateFrom = new Date(dateFrom.value);
      dateTo = new Date(dateTo.value);
      return visits.filter(visit => {
        const visitDate = new Date(visit.visitDate);
         return (visitDate >= dateFrom && visitDate <= dateTo);
      })
    }

    createCard = (e) => {
        switch(this.doctor) {
            case this.CARDIOLOGIST:
                    this.newVisitData = {
                    purpose: `${this.visitFormPurpose.value}`,
                    description: `${this.visitFormDescription.value.trim()}`,
                    urgency: `${this.visitFormUrgency.value}`,
                    firstName: `${this.visitFormName.value}`,
                    lastName: `${this.visitFormSurname.value}`,
                    middleName: `${this.visitFormMiddleName.value}`,
                    doctor: `${this.doctor}`,
                    visitDate: `${this.visitFormVisitDate.value}`,
                    age: `${this.visitFormAge.value}`,
                    pressure: `${this.visitFormPressureTop.value}` + '/' + `${this.visitFormPressureBottom.value}`,
                    waightIndex: `${this.visitFormBodyMassIndex.value}`,
                    diseases: `${this.visitFormCardioTroubles.value.trim()}`
                }
            break;
            case this.DENTIST:
                    this.newVisitData = {
                    purpose: `${this.visitFormPurpose.value}`,
                    description: `${this.visitFormDescription.value.trim()}`,
                    urgency: `${this.visitFormUrgency.value}`,
                    firstName: `${this.visitFormName.value}`,
                    lastName: `${this.visitFormSurname.value}`,
                    middleName: `${this.visitFormMiddleName.value}`,
                    doctor: `${this.doctor}`,
                    visitDate: `${this.visitFormVisitDate.value}`,
                    lastVisitData: `${this.visitFormLastVisitDate}`
                }
            break;
            case this.THERAPAPIST:
                    this.newVisitData = {
                    purpose: `${this.visitFormPurpose.value}`,
                    description: `${this.visitFormDescription.value.trim()}`,
                    urgency: `${this.visitFormUrgency.value}`,
                    firstName: `${this.visitFormName.value}`,
                    lastName: `${this.visitFormSurname.value}`,
                    middleName: `${this.visitFormMiddleName.value}`,
                    doctor: `${this.doctor}`,
                    visitDate: `${this.visitFormVisitDate.value}`,
                    age: `${this.visitFormAge.value}`
            }
            break;
        }

        this.visitForm.parentNode.removeChild(this.visitForm);
        const resp = this.request.post(JSON.stringify(this.newVisitData));
        resp.then(resp => {
            if (resp.statusText == 'OK') {
                resp.json().then(resp => {
                    let visit = {};
                    switch (this.doctor) {
                        case this.CARDIOLOGIST:
                            new this.VisitCardiologist(
                                this.request,
                                this.cardBord,
                                resp.id,
                                this.doctor,
                                this.visitFormVisitDate.value,
                                this.visitFormPurpose.value,
                                this.visitFormDescription.value.trim(),
                                this.visitFormUrgency.value,
                                this.visitFormName.value,
                                this.visitFormSurname.value,
                                this.visitFormMiddleName.value,
                                this.visitFormBodyMassIndex.value,
                                this.visitFormPressureTop.value,
                                this.visitFormCardioTroubles.value.trim(),
                                this.visitFormAge.value
                            );
                            break;

                        case this.DENTIST:
                            new this.VisitDentist(
                                this.request,
                                this.cardBord,
                                resp.id,
                                this.doctor,
                                this.visitFormVisitDate.value,
                                this.visitFormPurpose.value,
                                this.visitFormDescription.value.trim(),
                                this.visitFormUrgency.value,
                                this.visitFormName.value,
                                this.visitFormSurname.value,
                                this.visitFormMiddleName.value,
                            );
                            break;

                        case this.THERAPAPIST:
                            new this.VisitTherapist(
                                this.request,
                                this.cardBord,
                                resp.id,
                                this.doctor,
                                this.visitFormVisitDate.value,
                                this.visitFormPurpose.value,
                                this.visitFormDescription.value.trim(),
                                this.visitFormUrgency.value,
                                this.visitFormName.value,
                                this.visitFormSurname.value,
                                this.visitFormMiddleName.value,
                                this.visitFormAge.value
                            );
                            break;
                    }
                });
            }
        })
    }
    showRelevantFields = (e) => {
        switch (e.target.value){
            case this.CARDIOLOGIST:
                this.doctor = this.CARDIOLOGIST;
                this.visitFormPressureBottomLabel.style.display = 'block';
                this.visitFormPressureBottom.style.display = 'block';
                this.visitFormPressureTop.style.display = 'block';
                this.visitFormBodyMassIndexLabel.style.display = 'block';
                this.visitFormBodyMassIndex.style.display = 'block';
                this.visitFormCardioTroublesLabel.style.display = 'block';
                this.visitFormCardioTroubles.style.display = 'block';
                this.visitFormAgeLabel.style.display = 'block';
                this.visitFormAge.style.display = 'block';
                this.visitFormLastVisitDate.style.display = 'none';
                this.visitFormLastVisitDateLabel.style.display = 'none'
                break;

            case this.DENTIST:
                this.doctor = this.DENTIST;
                this.visitFormLastVisitDate.style.display = 'block';
                this.visitFormLastVisitDateLabel.style.display = 'block';
                this.visitFormPressureBottomLabel.style.display = 'none';
                this.visitFormPressureBottom.style.display = 'none';
                this.visitFormPressureTop.style.display = 'none';
                this.visitFormBodyMassIndexLabel.style.display = 'none';
                this.visitFormBodyMassIndex.style.display = 'none';
                this.visitFormCardioTroublesLabel.style.display = 'none';
                this.visitFormCardioTroubles.style.display = 'none';
                this.visitFormAgeLabel.style.display = 'none';
                this.visitFormAge.style.display = 'none';
                break;

            case this.THERAPAPIST:
                this.doctor = this.THERAPAPIST;
                this.visitFormAgeLabel.style.display = 'block';
                this.visitFormAge.style.display = 'block';
                this.visitFormPressureBottomLabel.style.display = 'none';
                this.visitFormPressureBottom.style.display = 'none';
                this.visitFormPressureTop.style.display = 'none';
                this.visitFormBodyMassIndexLabel.style.display = 'none';
                this.visitFormBodyMassIndex.style.display = 'none';
                this.visitFormCardioTroublesLabel.style.display = 'none';
                this.visitFormCardioTroubles.style.display = 'none';
                this.visitFormLastVisitDate.style.display = 'none';
                this.visitFormLastVisitDateLabel.style.display = 'none';
                break;
        }
    }

    closeVisit (e) {
        e.target.parentNode.parentNode.parentNode.removeChild(e.target.parentNode.parentNode);
    }

   renderModal = () => {
        this.board.style.display = 'block';
        this.root.append(this.board);
        this.getAllVisits().then(visits => {
          this.renderVisits(visits);
        }
        );
    }

  getAllVisits = () => {
    const response = this.request.getAll();
    return response.then(response => response.json())
      .then(visits => {
        return this.visitsResponse = visits
      })
  }

    renderVisits = () => {
        if (this.visits.length == 0){
          this.buildVisits();
        }else{
        }
    }

    buildVisits(){
      this.visits = [];
        for (const visit of this.visitsResponse) {
            switch (visit.doctor) {
                case this.CARDIOLOGIST:
                    const visitCardiologist = new this.VisitCardiologist(
                        this.request,
                        this.cardBord,
                        visit.id,
                        visit.doctor,
                        visit.visitDate,
                        visit.purpose,
                        visit.description,
                        visit.urgency,
                        visit.firstName,
                        visit.lastName,
                        visit.middleName,
                        visit.waightIndex,
                        visit.pressure,
                        visit.diseases,
                        visit.age
                        );
                        this.visits.push(visitCardiologist);
                     break;
                case this.DENTIST:
                    const visitDentist = new this.VisitDentist(
                        this.request,
                        this.cardBord,
                        visit.id,
                        visit.doctor,
                        visit.visitDate,
                        visit.purpose,
                        visit.description,
                        visit.urgency,
                        visit.firstName,
                        visit.lastName,
                        visit.middleName,
                        visit.lastVisitDate
                        );
                        this.visits.push(visitDentist);
                    break;
                case this.THERAPAPIST:
                    const visitTherapist = new this.VisitTherapist(
                        this.request,
                        this.cardBord,
                        visit.id,
                        visit.doctor,
                        visit.visitDate,
                        visit.purpose,
                        visit.description,
                        visit.urgency,
                        visit.firstName,
                        visit.lastName,
                        visit.middleName,
                        visit.age
                        );
                        this.visits.push(visitTherapist)
                    break;
            }
        }

      if (this.visits.length == 0) {
        alert('no cards')
      }
    }

    createVisit = () => {
        this.root.append(this.visitForm);
    }
}
