'use strict'
export default class Authorization{
    constructor(root, modal, request){
        this.root = root;
        this.modal = modal;
        this.request = request;
        this.initialisation();
    }

    initialisation(){
        this.loginForm = document.getElementById('login-form').content.children[0];
        this.loginButton = document.getElementsByClassName('header__login-button')[0];
        this.form = this.loginForm.cloneNode(true);
        this.loginButton.addEventListener('click', this.loginButtonClick);
        this.form.submit.addEventListener('click', this.submitLoginForm);
        this.board = document.getElementById('board').content.children[0];
        this.checkAuthorization();
    }

    checkAuthorization(){
        const token = localStorage.getItem('token');
        if(token){
            this.request.setTokin(token);
            this.modal.renderModal(this.modal.visits);
            this.modal.visits = [];
            this.loginButton.textContent = 'logout';
        }
    }

    loginButtonClick = (e) => {
        if(this.loginButton.textContent === 'login'){
            this.loginAction();
        }else
            this.logoutAction();
    }

    loginAction(){
        this.loginButton.textContent = 'logout';
        this.form.style.display = 'block';
        this.root.appendChild(this.form);
    }

    logoutAction(){
        if(this.loginButton.textContent === 'logout')
        {
            this.loginButton.textContent = 'login';
            localStorage.removeItem('token');
            this.board.parentNode.removeChild(this.board);
            location.reload();
        }
    }

    submitLoginForm = (e) => {
        e.preventDefault();
        let login = this.form.uname.value;
        let password = this.form.psw.value;
            const token = this.request.getToken(login, password);
            token.then(response =>{
                if(response.ok == true){
                    token
                    .then(response => response.text())
                    .then(token => {
                        this.request.setTokin(token);
                        localStorage.setItem('token', token);
                        this.form.style.display = 'none';
                        this.modal.renderModal();
                    })
                }else{
                    console.log('wrong credentials!!!');
                }
            });
    }
}
            //@ToDo Need to add check by correct login and password and correct response
