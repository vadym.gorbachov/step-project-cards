'use strict'

import Request from './modules/Request.js';
import Authorisation from './modules/Authorization.js';
import Modal from "./modules/Modal.js";
import {root, BASE_URL, doctorsTypes as doctorsTypes} from "./modules/constants.js";
import {VisitCardiologist, VisitDentist, VisitTherapist} from './modules/cards/Visit.js';

const doRequest = new Request(BASE_URL);
const modal = new Modal(root, doRequest, doctorsTypes,
    VisitCardiologist, VisitDentist, VisitTherapist
    );
new Authorisation(root, modal, doRequest);





